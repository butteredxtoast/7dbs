$ psql 7dbs

CREATE TABLE countries (
  country_code char(2) PRIMARY KEY,
  country_name text UNIQUE
);

INSERT INTO countries (country_code, country_name)
VALUES ('us','United States'),('mx', 'Mexico'),('au','Australia'),('gb','United Kingdom'),('de',
'Germany'),('ll','Loompaland');

DELETE FROM countries
WHERE country_code = 'll';

CREATE TABLE cities (
  name text NOT NULL,
  postal_code varchar(9) CHECK (postal_code <> ''),
  country_code char(2) REFERENCES countries,
  PRIMARY KEY (country_code, postal_code)
);

INSERT INTO cities
VALUES ('Toronto','M4C1B5','ca');

INSERT INTO cities
VALUES ('Portland','87200','us');

UPDATE cities
SET postal_code = '97206'
WHERE name = 'Portland';

CREATE TABLE venues (
  venue_id SERIAL PRIMARY KEY,
  name varchar(255),
  street_address text,
  type char(7) CHECK ( type in ('public','private')) DEFAULT 'public',
  postal_code varchar(9),
  country_code char(2),
  FOREIGN KEY (country_code, postal_code)
  REFERENCES cities (country_code, postal_code) MATCH FULL
);

INSERT INTO venues (name, postal_code, country_code)
VALUES ('Crystal Ballroom','97206','us');

INSERT INTO venues (name, postal_code, country_code)
VALUES ('Voodoo Doughnut', '97206', 'us') RETURNING venue_id;

CREATE TABLE events (
  event_id SERIAL PRIMARY KEY,
  title varchar (255),
  starts timestamp,
  ends timestamp,
  venue_id int,
  FOREIGN KEY (venue_id)
  REFERENCES venues (venue_id) MATCH FULL
);

INSERT INTO events (title, starts, ends, venue_id)
  VALUES ('Fight Club', '2018-02-15 17:30:00', '2018-02-15 19:30:00', '2');

INSERT INTO events (title, starts, ends)
  VALUES ('April Fools Day', '2018-04-01 00:00:00', '2018-04-01 23:59:59');

INSERT INTO events (title, starts, ends)
  VALUES ('Christmas Day', '2018-12-25 00:00:00', '2018-12-25 23:59:59');

CREATE INDEX events_title
  ON events USING hash (title);

CREATE INDEX events_starts
  ON events USING btree (starts);

INSERT INTO events (title, starts, ends, venue_id)
  VALUES ('Moby', '2018-02-06 21:00','2018-02-06 23:00', (
    SELECT venue_id
    FROM venues
    WHERE name = 'Crystal Ballroom'
  )
);

INSERT INTO events (title, starts, ends, venue_id)
  VALUES ('Wedding', '2018-02-26 21:00', '2018-02-26 23:00', (
    SELECT venue_id
    FROM venues
    WHERE name = 'Voodoo Doughnut'
  )
);

INSERT INTO events (title, starts, ends, venue_id)
  VALUES ('Dinner with Mom', '2018-02-26 18:00', '2018-02-26 20:30', (
    SELECT venue_id
    FROM venues
    WHERE name = 'My Place'
  )
);

INSERT INTO events (title, starts, ends)
  VALUES ('Valentine''s Day', '2018-02-14 00:00', '2018-02-14 23:59');

SELECT count(title)
FROM events
WHERE title LIKE '%Day%';

SELECT min(starts), max(ends)
FROM events INNER JOIN venues
ON events.venue_id = venues.venue_id
WHERE venues.name = 'Crystal Ballroom';

SELECT venue_id, count(*)
FROM events
GROUP BY venue_id;

SELECT venue_id
FROM events
GROUP BY venue_id
HAVING count(*) >= 2 AND venue_id IS NOT NULL;

SELECT venue_id
FROM events
GROUP BY venue_id;

SELECT title, venue_id, count(*)
FROM events
GROUP BY venue_id;

SELECT title, count(*) OVER (PARTITION BY venue_id) FROM events;

BEGIN TRANSACTION
DELETE FROM events;
ROLLBACK;
SELECT * FROM events;

SELECT add_event('House Party', '2018-05-03 23:00', '2018-05-04 02:00', 'Run''s House', '97206', 'us');


CREATE TABLE logs (
  event_id integer,
  old_title varchar(255),
  old_starts timestamp,
  old_ends timestamp,
  logged_at timestamp DEFAULT current_timestamp
);

UPDATE events
  SET ends='2018-05-04 01:00:00'
  WHERE title='House Party';

SELECT add_event('Valentine''s Day', '2018-02-14 00:00', '2018-02-14 23:59');

SELECT name, to_cahr(date, 'Month DD, YYYY') AS date
FROM holidays
WHERE date <= '2018-04-01';

ALTER TABLE events
ADD colors text ARRAY;

CREATE OR REPLACE VIEW holidays AS
  SELECT event_id AS holiday_id, title AS name, starts AS date, colors
  FROM events
  WHERE title LIKE '%Day%' AND venue_id IS NULL;

UPDATE holidays SET colors = '{"red","green"}' where name = 'Christmas Day';
-- the book said the above wasn't supposed to work, but it did

SELECT extract(year from starts) as year,
  extract(month from starts) as month, count(*)
FROM events
GROUP BY year, month
ORDER BY year, month;

CREATE TEMPORARY TABLE month_count(month INT);
INSERT INTO month_count VALUES (1),(2),(3),(4),(5),(6),(7),(8),(9),(10),(11),(12);

SELECT * FROM crosstab(
  'SELECT extract(year from starts) as year,
  extract(month from starts) as month, count(*)
  FROM events
  GROUP BY year, month
  ORDER BY year, month',
  'SELECT * FROM month_count'
) AS (
  year int,
  jan int, feb int, mar int, apr int, may int, jun int, jul int, aug int, sep int, oct int, nov int, dec int
) ORDER BY YEAR;
